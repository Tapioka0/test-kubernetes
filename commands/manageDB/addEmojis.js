const ms = require("ms");
const { constructorEmbed } = require("../../helpers/utils");

const Emojis = require("../../Models/Emojis");

module.exports = {
  name: "addemojis",
  descripcion: "add",
  aliases: [""],
};

module.exports.run = async (client, msg, args, count) => {
  if (!args[0] && !args[1])
    return msg.reply("**Uso incorrecto del comando**").then((msg) => {
      setTimeout(() => msg.delete(), 3000);
    });
  if (!msg.member.roles.cache.some((role) => role.id == "858485138869846026"))
    return console.log("a");

  try {
    // await Emojis.create({ emojis: [{ name: args[1], tipo: args[0] }] });
    const date = await Emojis.findOne({
      emojis: { $exists: true },
    });

    date.emojis.push({ name: args[1], tipo: args[0] });
    await date.save();

    msg.react("👍");
  } catch (e) {
    console.log(e);
    msg.channel.send("ocurrio un error");
  }
};
