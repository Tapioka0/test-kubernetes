"use strict";

const { compose } = require("../helpers/utils.js");

let prefix = "!";

module.exports.run = async (client, message) => {
  if (!message.content.startsWith(prefix)) return;
  const separarArray = (msg) => msg.content.split(" ");
  const headDelArray = (array) => array[0];
  const obtenerCommand = (comando) =>
    client.comandos.get(comando.slice(prefix.length)) ||
    client.comandos.find(
      (cmd) => cmd.aliases && cmd.aliases.includes(comando.slice(prefix.length))
    );

  const args = (msg) => separarArray(msg).slice(1);

  const ejecutarComando = (comando) => {
    if (comando) comando.run(client, message, args(message), client.count);
  };
  compose(ejecutarComando, obtenerCommand, headDelArray, separarArray)(message);
};
