const ms = require("ms");
const { constructorEmbed } = require("../../helpers/utils");
const randomWords = require("random-spanish-words");
module.exports = {
  name: "easymode",
  descripcion: "60s",
  aliases: ["easy"],
};

module.exports.run = async (client, msg, args, count) => {
  if (count)
    return msg
      .reply("**Ya me encuentro en uso, espere unos segundos**")
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });

  let params = args[0] ?? 60;
  if (args[0] >= 300 || args[0] <= 0)
    return msg
      .reply(
        "**El tiempo limite es de 300s, porfavor escribe un numero menor a 300s**"
      )
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });

  let embed = constructorEmbed({
    setTitle: "EASYMODE",
    setDescription: `**-  ${params}  -**`,
    setFooter: {
      text: "easymode",
    },
  });
  let menssage = await msg.channel.send({ embeds: [embed] });
  await menssage.react("▶️");

  const filter = (reaction, user) => {
    return (
      (reaction.emoji.name === "▶️" && user.id === msg.author.id) ||
      (reaction.emoji.name === "⏸️" && user.id === msg.author.id) ||
      (reaction.emoji.name === "🔁" && user.id === msg.author.id) ||
      (reaction.emoji.name === "❌" && user.id === msg.author.id)
    );
  };
  const collector = menssage.createReactionCollector({
    filter,
    time: 100000,
    errors: ["time"],
  });
  let pause = false;
  let palabra = randomWords().toUpperCase();

  collector.on("collect", (collected) => {
    if (collected.emoji.name === "⏸️") {
      pause = true;
      menssage.reactions.removeAll();
      menssage.react("▶️");
      menssage.react("🔁");
      menssage.react("❌");
    }
    if (collected.emoji.name === "🔁") {
      params = args[0] ?? 60;
      menssage.reactions.cache.get("🔁").remove();
      palabra = randomWords().toUpperCase();
      const embeda = constructorEmbed({
        setTitle: "EASYMODE",
        setDescription: `**-  ${params}  -**`,
        setFooter: {
          text: "easymode",
        },
      });
      menssage.edit({ embeds: [embeda] });
    }
    if (collected.emoji.name === "▶️") {
      menssage.reactions.removeAll();
      if (pause) {
        pause = false;
      } else {
        edit();
        client.count = 1;
      }

      menssage.react("⏸️");
    }
    if (collected.emoji.name === "❌") {
      menssage.delete();
      client.count = 0;
    }
  });

  collector.on("end", (collected) => {
    client.count = 0;
  });

  const edit = () => {
    if (pause) return setTimeout(() => edit(), 1050);
    params = params - 1;

    if (params % 10 == 0) {
      palabra = randomWords().toUpperCase();
    }
    if (params == 0) {
      palabra = "TIEMPO!";
      params = 0;
      client.count = 0;
    }
    const embedss = constructorEmbed({
      setTitle: palabra,
      setDescription: `**-  ${params}  -**`,
      setFooter: {
        text: "easymode",
      },
    });
    menssage.edit({ embeds: [embedss] }).catch(() => {});
    if (params <= 0) return menssage.delete();

    return setTimeout(() => edit(), 1050);
  };
};
