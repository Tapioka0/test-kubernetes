"use strict";

const { Client, Collection } = require("discord.js");
const client = new Client({
  intents: [
    "GUILDS",
    "GUILD_MESSAGES",
    "DIRECT_MESSAGES",
    "GUILD_VOICE_STATES",
    "DIRECT_MESSAGE_REACTIONS",
    "GUILD_MESSAGE_REACTIONS",
    "GUILD_MESSAGE_TYPING",
    "DIRECT_MESSAGES",
    "GUILD_BANS",
    "GUILD_EMOJIS_AND_STICKERS",
    "GUILD_INTEGRATIONS",
    "GUILD_MEMBERS",
    "GUILD_SCHEDULED_EVENTS",
    "GUILD_WEBHOOKS",
  ],
});
const { token } = require("./config.json");
const conectarDB = require("./database/characters");
const { buscarArchivos } = require("./helpers/utils");

conectarDB();
client.comandos = new Collection();
client.count = 0;
const comandos = (Files) =>
  buscarArchivos(Files).map((x) => {
    if (x.name.endsWith(".js")) {
      let todosLosDatos = require(`${Files}/${x.name}`);
      client.comandos.set(todosLosDatos.name, todosLosDatos);
    } else if (x.isDirectory) comandos(`${Files}/${x.name}`);
  });

const eventos = (Files) =>
  buscarArchivos(Files).map((x) =>
    client.on(x.name.split(".")[0], (...args) =>
      require(`${Files}/${x.name}`).run(client, ...args)
    )
  );
console.log(process.env.token);

eventos("./events");
comandos("./commands");
client.login(process.env.token);
