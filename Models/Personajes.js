const mongoose = require("mongoose");

const PersonajesSchema = mongoose.Schema({
  personajes: [
    {
      name: String,
      tipo: String,
    },
  ],
});

module.exports = mongoose.model("Personajes", PersonajesSchema);
