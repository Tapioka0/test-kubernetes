const ms = require("ms");
const { constructorEmbed } = require("../../helpers/utils");
const randomWords = require("random-spanish-words");

module.exports = {
  name: "terminaciones",
  descripcion: "terminaciones",
  aliases: [],
};

module.exports.run = async (client, msg, args, count) => {
  if (count)
    return msg
      .reply("**Ya me encuentro en uso, espere unos segundos**")
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });

  let params = args[1] ?? 120;
  if (args[0] > 60 || args[0] < 4)
    return msg
      .reply(
        "**El tiempo minimo es de 4s, porfavor escribe un numero menor a 60s**"
      )
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });
  if (args[1] > 300 || args[1] % 2 !== 0)
    return msg
      .reply(
        "**El tiempo maximo es de 300s, porfavor escribe un numero menor a 300s y sea par**"
      )
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });
  if (!args[0])
    return msg.reply("**Ingrese un numero par**").then((msg) => {
      setTimeout(() => msg.delete(), 3000);
    });
  if (args[0] % 2 !== 0)
    return msg.reply("**El tiempo debe ser un numero par**").then((msg) => {
      setTimeout(() => msg.delete(), 3000);
    });

  let embed = constructorEmbed({
    setTitle: "TERMINACIONES",
    setDescription: `**-  ${params}  -**`,
    setFooter: {
      text: "terminaciones",
    },
  });
  let menssage = await msg.channel.send({ embeds: [embed] });
  await menssage.react("▶️");

  const filter = (reaction, user) => {
    return (
      (reaction.emoji.name === "▶️" && user.id === msg.author.id) ||
      (reaction.emoji.name === "⏸️" && user.id === msg.author.id) ||
      (reaction.emoji.name === "🔁" && user.id === msg.author.id) ||
      (reaction.emoji.name === "❌" && user.id === msg.author.id)
    );
  };
  const collector = menssage.createReactionCollector({
    filter,
    time: 1000000,
    errors: ["time"],
  });
  const ramdonWord = () =>
    characters[Math.floor(Math.random() * characters.length)];
  let pause = false;
  let palabra1 = ramdonWord();
  let palabra2 = ramdonWord();
  let palabra = `${
    palabra1.slice(0, palabra1.length - 3) +
    "**" +
    palabra1.slice(palabra1.length - 3) +
    "**"
  }  ${
    palabra2.slice(0, palabra2.length - 3) +
    "**" +
    palabra2.slice(palabra2.length - 3) +
    "**"
  }`;

  collector.on("collect", (collected) => {
    if (collected.emoji.name === "⏸️") {
      pause = true;
      menssage.reactions.removeAll();
      menssage.react("▶️");
      menssage.react("🔁");
      menssage.react("❌");
    }
    if (collected.emoji.name === "🔁") {
      params = args[1] ?? 120;
      menssage.reactions.cache.get("🔁").remove();
      palabra = coolImages.one(600, 800);
      let embeda = constructorEmbed({
        setTitle: "TERMINACIONES",
        setDescription: `**-  ${params}  -**`,
        setFooter: {
          text: "terminaciones",
        },
      });
      menssage.edit({ embeds: [embeda] });
    }
    if (collected.emoji.name === "▶️") {
      menssage.reactions.removeAll();
      if (pause) {
        pause = false;
      } else {
        edit();
        client.count = 1;
      }

      menssage.react("⏸️");
    }
    if (collected.emoji.name === "❌") {
      menssage.delete();
      client.count = 0;
    }
  });
  collector.on("end", (collected) => {
    client.count = 0;
  });
  const edit = () => {
    if (pause) return setTimeout(() => edit(), 1050);
    params = params - 1;

    if (params % args[0] == 0) {
      let palabra3 = ramdonWord();
      let palabra4 = ramdonWord();
      palabra = `${
        palabra3.slice(0, palabra3.length - 3) +
        "**" +
        palabra3.slice(palabra3.length - 3) +
        "**"
      }  ${
        palabra4.slice(0, palabra4.length - 3) +
        "**" +
        palabra4.slice(palabra4.length - 3) +
        "**"
      }`;
    }
    if (params == 0) {
      // palabra = "TIEMPO!";
      params = 0;
      client.count = 0;
    }
    let embedss = constructorEmbed({
      setTitle: `TERMINACIONES`,
      setDescription: `${palabra} \n\n**-  ${params}  -**`,
      setFooter: {
        text: `terminaciones`,
      },
    });
    menssage.edit({ embeds: [embedss] }).catch(() => {});
    if (params <= 0) return menssage.delete();

    return setTimeout(() => edit(), 1070);
  };
};
