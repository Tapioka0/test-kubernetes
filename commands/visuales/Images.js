const ms = require("ms");
const { constructorEmbed } = require("../../helpers/utils");
const randomWords = require("random-spanish-words");
const coolImages = require("cool-images");
module.exports = {
  name: "image",
  descripcion: "imagenes",
  aliases: [],
};

module.exports.run = async (client, msg, args, count) => {
  if (count)
    return msg
      .reply("**Ya me encuentro en uso, espere unos segundos**")
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });

  let params = args[1] ?? 120;
  if (args[0] > 60 || args[0] < 4)
    return msg
      .reply(
        "**El tiempo minimo es de 4s, porfavor escribe un numero menor a 60s**"
      )
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });
  if (args[1] > 300 || args[1] % 2 !== 0)
    return msg
      .reply(
        "**El tiempo maximo es de 300s, porfavor escribe un numero menor a 300s y sea par**"
      )
      .then((msg) => {
        setTimeout(() => msg.delete(), 3000);
      });
  if (!args[0])
    return msg.reply("**Ingrese un numero par**").then((msg) => {
      setTimeout(() => msg.delete(), 3000);
    });
  if (args[0] % 2 !== 0)
    return msg.reply("**El tiempo debe ser un numero par**").then((msg) => {
      setTimeout(() => msg.delete(), 3000);
    });

  let embed = constructorEmbed({
    setTitle: "IMAGEMODE",
    setDescription: `**-  ${params}  -**`,
    setFooter: {
      text: "IMAGEMODE",
    },
  });
  let menssage = await msg.channel.send({ embeds: [embed] });
  await menssage.react("▶️");

  const filter = (reaction, user) => {
    return (
      (reaction.emoji.name === "▶️" && user.id === msg.author.id) ||
      (reaction.emoji.name === "⏸️" && user.id === msg.author.id) ||
      (reaction.emoji.name === "🔁" && user.id === msg.author.id) ||
      (reaction.emoji.name === "❌" && user.id === msg.author.id)
    );
  };
  const collector = menssage.createReactionCollector({
    filter,
    time: 1000000,
    errors: ["time"],
  });

  let pause = false;
  let palabra = coolImages.one(500, 800);

  collector.on("collect", (collected) => {
    if (collected.emoji.name === "⏸️") {
      pause = true;
      menssage.reactions.removeAll();
      menssage.react("▶️");
      menssage.react("🔁");
      menssage.react("❌");
    }
    if (collected.emoji.name === "🔁") {
      params = args[1] ?? 120;
      menssage.reactions.cache.get("🔁").remove();
      palabra = coolImages.one(600, 800);
      let embeda = constructorEmbed({
        setTitle: "IMAGEMODE",
        setDescription: `**-  ${params}  -**`,
        setFooter: {
          text: "IMAGE",
        },
      });
      menssage.edit({ embeds: [embeda] });
    }
    if (collected.emoji.name === "▶️") {
      menssage.reactions.removeAll();
      if (pause) {
        pause = false;
      } else {
        edit();
        client.count = 1;
      }

      menssage.react("⏸️");
    }
    if (collected.emoji.name === "❌") {
      menssage.delete();
      client.count = 0;
    }
  });
  collector.on("end", (collected) => {
    client.count = 0;
  });
  const edit = () => {
    if (pause) return setTimeout(() => edit(), 1050);
    params = params - 1;

    if (params % args[0] == 0) {
      palabra = coolImages.one(600, 800);
    }
    if (params == 0) {
      // palabra = "TIEMPO!";
      params = 0;
      client.count = 0;
    }
    let embedss = constructorEmbed({
      setTitle: `IMÁGENES`,
      setDescription: `**-  ${params}  -**`,
      setImage: `${palabra}`,
      setFooter: {
        text: `ImageMode`,
      },
    });
    menssage.edit({ embeds: [embedss] }).catch(() => {});
    if (params <= 0) return menssage.delete();

    return setTimeout(() => edit(), 1070);
  };
};
