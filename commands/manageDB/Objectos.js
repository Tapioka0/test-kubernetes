const ms = require("ms");
const { constructorEmbed } = require("../../helpers/utils");

const Objectos = require("../../Models/Objectos");

module.exports = {
  name: "addobjectos",
  descripcion: "add",
  aliases: [""],
};

module.exports.run = async (client, msg, args, count) => {
  if (!args[0] && !args[1])
    return msg.reply("**Uso incorrecto del comando**").then((msg) => {
      setTimeout(() => msg.delete(), 3000);
    });
  if (!msg.member.roles.cache.some((role) => role.id == "858485138869846026"))
    return console.log("a");

  try {
    const date = await Objectos.findOne({
      Objetos: { $exists: true },
    });

    date.Objectos.push({ name: args[1], tipo: args[0] });
    await date.save();

    msg.react("👍");
  } catch (e) {
    console.log(e);
    msg.channel.send("ocurrio un error");
  }
};
