const mongoose = require("mongoose");

const ObjectosSchema = mongoose.Schema({
  Objectos: [
    {
      name: String,
      tipo: String,
    },
  ],
});

module.exports = mongoose.model("Objectos", ObjectosSchema);
