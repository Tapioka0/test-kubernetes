"user strict";

module.exports.run = async (client) => {
  console.log(`
    ____________________\n
    Client: ${client.user.tag}\n
    Client ID: ${client.user.id}\n
    Guild Count: ${client.guilds.cache.size}\n
    User Count: ${client.users.cache.size}\n
    ____________________`);

  client.user.setActivity(`online`, { type: "WATCHING" });
};
