const mongoose = require("mongoose");

const EmojisSchema = mongoose.Schema({
  emojis: [
    {
      name: String,
      tipo: String,
    },
  ],
});

module.exports = mongoose.model("Emojis", EmojisSchema);
