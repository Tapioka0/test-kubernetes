const fs = require("fs");

const { MessageEmbed } = require("discord.js");

const compose =
  (...fns) =>
  (x) =>
    fns.reduceRight((y, f) => f(y), x);

const buscarArchivos = (path) => fs.readdirSync(path, { withFileTypes: true });
const readFile = (path) => fs.readFileSync(path, "utf8");
const borrarFile = (path) => fs.unlinkSync(path);

const randomNumber = (limit) => {
  return Math.floor(Math.random() * limit).toString();
};

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const enviarPedasosMsg = (data, message) => {
  const ly = data.match(/.{1,2000}/g);
  ly.forEach((chunk) => message.channel.send(chunk, { split: true }));
};
const siExiste = (option) => {
  if (option) return option;
  return "";
};
const constructorEmbed = (options) => {
  const embed = new MessageEmbed()

    .setTitle(siExiste(options.setTitle))
    .setURL(siExiste(options.setUrl))

    .setDescription(siExiste(options.setDescription))
    .setThumbnail(siExiste(options.setThumbnail))
    .setImage(siExiste(options.setImage))
    .setAuthor({
      name: siExiste(options.setAuthor?.name),
      iconURL: siExiste(options.setAuthor?.iconURL),
      url: siExiste(options.setAuthor?.url),
    })
    .setFooter({
      text: siExiste(options.setFooter?.text),
      iconURL: siExiste(options.setFooter?.iconURL),
    });
  if (options.addFields) embed.addFields(options.addFields);
  return embed;
};

module.exports = {
  buscarArchivos,
  compose,
  randomNumber,
  sleep,
  enviarPedasosMsg,
  readFile,
  borrarFile,
  constructorEmbed,
};
